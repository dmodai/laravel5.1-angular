<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole {

    //

    public function getIdByRolename($rolename){
       return Role::where('name',$rolename)->first()->id;
    }
}
<?php namespace App\Http\Controllers;

use App\Page;
//use Illuminate\Support\Facades\Auth;

use Redirect, Input, Auth,View;

class HomeController extends Controller {

    public function index(){
        if(Auth::guest()){
            return Redirect::to('auth/login');
    }else{
            //return view('index')->withPages(Page::all());
            //return view('app')->withPages(Page::all());
            return View::make('homeManage');
        }
    }

}
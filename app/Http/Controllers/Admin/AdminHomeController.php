<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Page,App\Role,App\Permission,App\User;
class AdminHomeController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
       /* $admin = new Role;
        $admin->name = 'superadmin';
        $role = Role::where('name','superadmin')->first();
        var_dump($admin->getIdByRolename('superadmin'));*/

       /* $admin = new Role;
        $admin->name = 'superadmin';
        $admin->id = 2;
        $owner = new Role;
        $owner->name = 'ordinary';
        $owner->id = 1;

        $manageUsers = new Permission;
        $manageUsers->name = 'manage_users';
        $manageUsers->display_name = 'Manage Users';
        $manageUsers->id = 1;
        //$manageUsers->save();

        $managePosts = new Permission;
        $managePosts->name = 'manage_pages';
        $managePosts->display_name = 'Manage Pages';
        $managePosts->id = 2;
        //$managePosts->save();
        $admin->perms()->sync(array($managePosts->id, $manageUsers->id));
        $owner->perms()->sync(array($managePosts->id),$owner->id);


        $user = User::where('name','=','dmodai')->first();

// 可以使用 Entrust 提供的便捷方法用户授权
// 注: 参数可以为 Role 对象, 数组, 或者 ID
        $user->attachRole( $admin );

// 或者使用 Eloquent 自带的对象关系赋值
      //  $user->roles()->attach( $admin->id ); // id only*/

        //return view('AdminHome')->withPages(Page::all());
        //return view('AdminHome')->with('pages', Page::all());
        return view('AdminHome', ['pages' => Page::all()]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}

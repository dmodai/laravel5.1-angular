<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\DB;
use App\Comment;

use Redirect, Input,Auth;
class CommentsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
        //paginate
        $contentFilter = Input::get('contentFilter');
        $comments = Comment::whereraw("content like ? ", ['%'.$contentFilter.'%'])->Paginate(10);
        return view('admin.comments.index', ['comments' => $comments, 'contentFilter' => $contentFilter]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
        //$comments = DB::table('comments')->where('page_id', $id)->get();
        $comments = Comment::where('page_id', $id)->get();
        return view('admin.comments.show', ['comments' => $comments]);
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
        return view('admin.comments.edit')->withComment(Comment::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		//
        $this->validate($request,[
            'nickname' => 'required',
            'content'  => 'required',
        ]);
       /* if(Comment::where('id', $id)->update(Input::expect(['_method', 'token']))){
            return Redirect::to('admin/comment');
        }else{
            return Redirect::back()->withInput()->withErrors('更新失败');
        }*/


        $this->validate($request, [
            'nickname' => 'required',
            'content' => 'required',
        ]);

        $comment = Comment::find($id);
        $comment->nickname = Input::get('nickname');
        $comment->content = Input::get('content');
        //$comment->user_id = 1;//Auth::user()->id;
        if ($request->ajax()){
            $returnInfo = array('success'=>0,'msg'=>'');
            if ($comment->save()) {
                $returnInfo['success']=1;
                $returnInfo['msg'] = '修改成功';
            }else{
                $returnInfo['msg'] = '修改失败';
            }
            return json_encode($returnInfo);
         }else{
            if ($comment->save()) {
                return Redirect::back()->withSuccess('更新成功');
            } else {
                return Redirect::back()->withInput()->withErrors('保存失败！');
            }
        }

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
        $comment = Comment::find($id);
        $comment->delete();

        return Redirect::to('admin/comments');
	}

}

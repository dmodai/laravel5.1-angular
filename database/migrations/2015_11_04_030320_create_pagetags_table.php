<?php
//php artisan make:model Article
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagetagsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		/*Schema::create('pages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
		});*/
        Schema::create('pagetags', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('type')->nullable();
            $table->integer('user_id');
            $table->integer('pid');
            $table->timestamps();
            $table->string('slug')->nullable();
        });

        // Create table for associating roles to users (Many-to-Many)
        Schema::create('page_tag', function (Blueprint $table) {
            $table->integer('page_id')->unsigned();
            $table->integer('tag_id')->unsigned();

            $table->foreign('page_id')->references('id')->on('pages')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('tag_id')->references('id')->on('pagetags')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['page_id', 'tag_id']);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages');
	}

}

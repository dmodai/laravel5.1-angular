<?php
/**
 * Date: 2015/11/4
 * Time: 11:13
 */
use Illuminate\Database\Seeder;
use App\Page;

class PageTableSeeder extends Seeder {

    public function run()
    {
        //DB::table('pages')->delete();

        for ($i=0; $i < 10; $i++) {
            Page::create([
                'title'   => 'Title '.$i,
                'body'    => 'Body '.$i,
                'user_id' => 1,
            ]);
        }
    }

}
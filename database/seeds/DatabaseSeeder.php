<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
       //导入对应seeder composer dump-autoload  php artisan db:seed
       // $this->call(PageTableSeeder::class);

        Model::reguard();
    }
}

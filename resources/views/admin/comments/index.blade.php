@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">管理评论</div>

                    <div class="panel-body">
                        <div class="search form-horizontal">
                            <div class="col-sm-10">
                                <input type="text" class="form-control col-sm-10" id="searchComment" value="{{$contentFilter}}"
                                       placeholder="请输入评论内容关键字">
                            </div>
                            <button class="col-md-2 btn btn-info">查找</button>

                        </div>
                        <table class="table table-striped">
                            <tr class="row">
                                <th class="col-lg-4">Content</th>
                                <th class="col-lg-2">User</th>
                                <th class="col-lg-4">Page</th>
                                <th class="col-lg-1">编辑</th>
                                <th class="col-lg-1">删除</th>
                            </tr>
                            @foreach ($comments as $comment)
                                <tr class="row">
                                    <td class="col-lg-6">
                                        {{ $comment->content }}
                                    </td>
                                    <td class="col-lg-2">
                                        @if ($comment->website)
                                            <a href="{{ $comment->website }}">
                                                <h4>{{ $comment->nickname }}</h4>
                                            </a>
                                        @else
                                            <h3>{{ $comment->nickname }}</h3>
                                        @endif
                                        {{ $comment->email }}
                                    </td>
                                    <td class="col-lg-4">
                                     {{--   <a href="{{ URL('pages/'.$comment->page_id) }}" target="_blank">
                                            {{ App\Page::find($comment->page_id)->title }}
                                        </a>--}}
                                    </td>
                                    <td class="col-lg-1">
                                        <a href="{{ URL('admin/comments/'.$comment->id.'/edit') }}" class="btn btn-success">编辑</a>
                                    </td>
                                    <td class="col-lg-1">
                                        <form action="{{ URL('admin/comments/'.$comment->id) }}" method="POST" style="display: inline;">
                                            <input name="_method" type="hidden" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button type="submit" class="btn btn-danger">删除</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>

                        <?php echo $comments->render(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function () {

            $(".search button").click(function(){
                var contentFilter = $("#searchComment").val();
                window.location.href = "{{URL('admin/comments')}}?contentFilter="+contentFilter;
            });
            $(".pagination li").on('click',function (event) {
                event.preventDefault();
                var contentFilter = $("#searchComment").val();
                window.location.href =$(this).children('a').attr('href')+"&contentFilter="+contentFilter;

            });
        });

    </script>
@endsection


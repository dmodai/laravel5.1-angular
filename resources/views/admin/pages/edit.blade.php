@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">编辑 Page</div>

                    <div class="panel-body">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="{{ URL('admin/pages/'.$page->id) }}" method="POST">
                            <input name="_method" type="hidden" value="PUT">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="text" name="title" class="form-control" required="required" value="{{ $page->title }}">
                            <br>
                            <textarea name="body" rows="10" class="form-control" required="required">{{ $page->body }}</textarea>
                            <br>
                            <button class="btn btn-lg btn-info">修改 Page</button>
                        </form>

                    </div>

                    <div class="comments" style="margin-top: 100px;">
                        @foreach ($page->hasManyComments as $comment)

                            <div class="one" style="border-top: solid 20px #efefef; padding: 5px 20px;">
                                <form action = "{{ URL('admin/comments/'.$comment->id) }}" method="post">
                                    <input name="_method" type="hidden" value="PUT">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="page_id" value="{{ $comment->page_id }}">

                                    <div class="nickname" data="{{ $comment->nickname }}">
                                        <input type="text" name="nickname" class="form-control col-md-8" required="required" value="{{ $comment->nickname }}">
                                        <h6>{{ $comment->created_at }}</h6>
                                    </div>
                                    <div class="content">
                                        <textarea name="content" rows="10" class="form-control" required="required">{{ $comment->content }}</textarea>
                                    </div>
                                    <button class="btn btn-lg btn-info commentMod">修改评论</button>
                                </form>
                                <div class="reply" style="text-align: right; padding: 5px;">
                                    <a href="#new" onclick="reply(this);">回复</a>
                                </div>
                            </div>



                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function () {
            $(".comments .one").each(function () {
                $(this).find('.commentMod').click(function(event){
                    event.preventDefault();
                    $.post(
                            $(this).parent('form').attr('action'),
                            {
                                '_method':'PUT',
                                '_token':$(this).siblings().eq(1).val(),
                                'page_id':$(this).siblings().eq(2).val(),
                                'nickname':$(this).siblings().eq(3).children().first().val(),
                                'content':$(this).siblings().eq(4).children().first().val(),
                            },
                            function (data) {
                                alert(data.msg);
                            },
                            'json'
                    );
                })
            })
        });

    </script>
@endsection


@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">show Page</div>

                    <div class="panel-body">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="{{ URL('admin/pages/'.$page->id) }}" method="POST">
                            <input name="_method" type="hidden" value="PUT">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <span>{{ $page->title }}</span>
                            <br>
                            <span>{{ $page->body }}</span>
                            <br>
                            <button class="btn btn-lg btn-info">查看 Page</button>
                        </form>

                    </div>

                    <div class="conmments" style="margin-top: 100px;">
                        @foreach ($page->hasManyComments as $comment)

                            <div class="one" style="border-top: solid 20px #efefef; padding: 5px 20px;">
                                <div class="nickname" data="{{ $comment->nickname }}">
                                    @if ($comment->website)
                                        <a href="{{ $comment->website }}">
                                            <h3>{{ $comment->nickname }}</h3>
                                        </a>
                                    @else
                                        <h3>{{ $comment->nickname }}</h3>
                                    @endif
                                    <h6>{{ $comment->created_at }}</h6>
                                </div>
                                <div class="content">
                                    <p style="padding: 20px;">
                                        {{ $comment->content }}
                                    </p>
                                </div>
                                <div class="reply" style="text-align: right; padding: 5px;">
                                    <a href="#new" onclick="reply(this);">回复</a>
                                </div>
                            </div>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection